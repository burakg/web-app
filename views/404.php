<?php
use burakg\ion AS ion;
?>
<div class="page-header">
	<div class="container">
		<h1>404</h1>
	</div>
</div>
<div class="container">
	<div class="jumbotron">
		<h2><?php echo ion\helpers\phraser::get()->translate('ERROR_404_TITLE');?></h2>
		<h4><?php echo ion\helpers\phraser::get()->translate('ERROR_404');?></h4>
	</div>
</div>