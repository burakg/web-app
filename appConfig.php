<?php
/**
 * App igniter
 * Call for the ION instance and setup routing, assets loading, etc...
 */

use burakg\ion\generic\appSetting;
use burakg\ion\ion;
use burakg\ion\front\navigation;
use burakg\ion\front\sitemap;

$settings = new appSetting(str_replace('\\','/',dirname(__FILE__)));

$settings
    ->set_cb_root(null)
    ->set_cf_root(null)
    ->set_cb_assets('_assets')
    ->set_cf_assets('website/_assets')
    ->set_assets_to_load([
        'css' => [
            'screen' => [
                'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Roboto:400,700',
                'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                $settings->CB_ASSETS.'css/ION.css',
            ],
            'print' => []
        ],
        'js' => [
            ['https://code.jquery.com/jquery-3.2.1.min.js',false],
            $settings->CB_ASSETS.'js/ION.js',
        ]
    ])
    ->set_masterpage_root('masterpages')
    ->set_data_root('data')
    ->set_sitemap_mode('default')
    ->set_root_authentication(false)
    ->set_auth_mode('default')
    ->load_app_settings()
    ->set_multi_lingual(true,'url');

$settings
    ->create_route(['GET'],'lingual-redirect/([0-9]*)', function($id){
        $args = func_get_args();
        $ion = ion::get();
        $ion->language()->set_current_lang($args[0]);
        $ion->curLang = $ion->language()->get_current_lang()->short;
        $ion->curLangPrefix = ($ion->get_current_app()->MULTI_LINGUAL === true) ? $ion->language()->get_current_lang()->short : null;

        $node = $ion->front()->start_frontend()->navigation()->node_url_by_attr('id',$args[1]);
        header('Location:'.$node);
    },true)
    ->create_route(['GET'],'sitemap',function(){
        header('Content-type: text/xml');

        $ion = ion::get();
        $ion
            ->language()
            ->set_app_language();

        $ion->front()
            ->start_frontend();

        $cache = $ion->get_current_app()->DATA_ROOT.'sitemap.xml';
        if(!file_exists($cache) || time()-filemtime($cache) > 3600){
            $xml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"></urlset>');

            $sm = sitemap::get()->node_by_attr('staticurl','');
            foreach($sm AS $node){
                $child = $xml->addChild('url');
                $child->addChild('loc',navigation::get()->node_url($node));
                $child->addChild('changefreq',($node['type'] == 1) ? 'weekly' : 'daily');
                $child->addChild('priority',($node['type'] == 1) ? '0.5' : '0.8');
            }

            $xml->asXML($cache);
        }
        exit(file_get_contents($cache));
    })
    ->create_route(['GET','POST'],'(.*)',function(){
        $args = func_get_args();
        $ion = ion::get();
        $ion
            ->language()
            ->set_app_language($args[0]);

        $ion->front()
            ->start_frontend();

        $fields = [
            'footermenu' => $ion->front()->navigation()->nav_nested('footerMenu'),
            'navigation' => $ion->front()->navigation()->nav_single_level('mainMenu'),
            'languagenav' => $ion->language()->language_nav(),
            'title' => $ion->front()->masterpage()->set_node_title(),
            'pagetitle' => $ion->front()->sitemap()->currentNode['title'],
            'sitetitle' => $ion->get_current_app()->get_setting('siteInfo')->title,
            'root' => function(){
                $ion = ion::get();
                $app = $ion->get_current_app();
                return ($app->MULTI_LINGUAL === true && $app->MULTI_LINGUAL_METHOD == 'url') ? CF_ROOT.$app->CF_ROOT.$ion->curLang.'/' : CF_ROOT.$app->CF_ROOT;
            },
            'absroot' => CF_ROOT,
            'url' => CF_URL,
            'curlang' => $ion->curLang,
            'assets' => $ion->get_current_app()->CF_ASSETS
        ];

        foreach($fields AS $key => $val)
            $ion->front()->masterpage()->set_template_fields($key,$val);

        echo $ion
            ->front()
            ->masterpage()
            ->ignite(true)
            ->end();
    },true)
    ->create_route(['GET','POST'],'/',function(){
        $ion = ion::get();
        $ion->language()->set_current_lang();
        header('Location: '.CF_ROOT.'/'.$ion->language()->curLang.'/');
    });

return $settings;