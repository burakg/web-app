<?php
use burakg\ion AS ion;
?>
<div class="jumbotron">
    <h2><?php echo 'Hello world!'; ?></h2>
    <h4>Congratulations, your web-app is up and running!<br>Now let's head to <a href="<?php echo ion\ion::get()->front()->navigation()->node_url_by_attr('id',2);?>">Quick Start</a></h4>
</div>